import characters from "../data/characters.json";
import { useParams } from "react-router-dom";

const Character = () => {
  const params = useParams();
  const character = characters.find(
    (el) => el.name === params.name.replace(/%20\s/g, " ")
  );
  return (
    <>
      <img src={character.image} alt="" />
      <div>Name: {character.name}</div>
      <div>Species: {character.species}</div>
      <div>Status: {character.status}</div>
      {character.type && <div>Type: {character.type}</div>}
    </>
  );
};

export default Character;
