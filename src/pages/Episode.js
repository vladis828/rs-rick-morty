import episodes from "../data/episode.json";
import { useParams } from "react-router-dom";

const Episode = () => {
  const params = useParams();
  const episode = episodes.find((el) => el.name === params.name);
  return (
    <>
      <div>Name: {episode.name}</div>
      <div>Air Date: {episode.air_date}</div>
      <div>Episode: {episode.episode}</div>
    </>
  );
};

export default Episode;
