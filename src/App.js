import { Routes, Route } from "react-router-dom";
import Main from "./pages/Main";
import Categories from "./pages/Categories";
import Character from "./pages/Character";
import Location from "./pages/Location";
import Episode from "./pages/Episode";
import SignIn from "./pages/SignIn";
import Auth from "./context/Auth";
import PrivateRoute from "./pages/PrivateRoute";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Auth>
          <Routes>
            <Route
              path="/"
              element={
                <PrivateRoute>
                  <Main />
                </PrivateRoute>
              }
            />

            <Route path="/signin" element={<SignIn />} />
            <Route
              path="categories/:category"
              element={
                <PrivateRoute>
                  <Categories />
                </PrivateRoute>
              }
            />
            <Route
              path="/characters/:name"
              element={
                <PrivateRoute>
                  <Character />
                </PrivateRoute>
              }
            />
            <Route
              path="/locations/:name"
              element={
                <PrivateRoute>
                  <Location />
                </PrivateRoute>
              }
            />
            <Route
              path="/episodes/:name"
              element={
                <PrivateRoute>
                  <Episode />
                </PrivateRoute>
              }
            />
          </Routes>
        </Auth>
      </header>
    </div>
  );
}

export default App;
