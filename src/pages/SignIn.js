import { useState, useRef } from "react";
import TextInput from "../inputs/TextInput";
import { useAuth } from "../context/Auth";
import { useNavigate } from "react-router-dom";

const SignIn = ({ signUpHandler }) => {
  const [inputForm, setInputForm] = useState({});
  const formRef = useRef(null);
  const auth = useAuth();
  const navigate = useNavigate();
  const onChange = (e) => {
    setInputForm((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const submitHandler = (e) => {
    e.preventDefault();
    auth.signIn(inputForm.email, () => {
      navigate("/");
    });
    formRef.current.reset();
  };

  const handleReset = () => {
    setInputForm({});
  };

  return (
    <>
      <form
        onSubmit={submitHandler}
        className="form"
        onChange={(e) => onChange(e)}
        ref={formRef}
        onReset={handleReset}
      >
        <TextInput
          type="email"
          label="Email:"
          name="email"
          placeholder="Enter email"
          required
        />
        <TextInput
          type="password"
          label="Password:"
          name="password"
          placeholder="Enter password"
          required
        />
        <input type="submit" value="Sign In" className="clickable"></input>
      </form>
      <div className="clickable"></div>
    </>
  );
};

export default SignIn;
