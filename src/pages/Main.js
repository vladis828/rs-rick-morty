import { Link } from "react-router-dom";

const Main = () => {
  return (
    <>
      <h3>Welcome to Rick & Morty world!</h3>
      <ul>
        <li>
          <Link to="/categories/characters">Characters</Link>
        </li>
        <li>
          <Link to="/categories/locations">Locations</Link>
        </li>
        <li>
          <Link to="/categories/episodes">Episodes</Link>
        </li>
      </ul>
    </>
  );
};

export default Main;
