function TextInput({ type, label, name, placeholder, ...props }) {
  return (
    <div>
      <label>{label}</label>
      <input type={type} placeholder={placeholder} name={name} {...props} />
    </div>
  );
}

export default TextInput;
