import locations from "../data/location.json";
import { useParams } from "react-router-dom";

const Location = () => {
  const params = useParams();
  const location = locations.find(
    (el) => el.name === params.name.replace(/%20\s/g, " ")
  );
  return (
    <>
      <div>Name: {location.name}</div>
      <div>Type: {location.type}</div>
      <div>Dimension: {location.dimension}</div>
    </>
  );
};

export default Location;
