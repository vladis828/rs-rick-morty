import { useAuth } from "../context/Auth";
import { Navigate } from "react-router-dom";

const PrivateRoute = ({ children }) => {
  const auth = useAuth();

  if (auth.user === null) return <Navigate to="/signin" />;
  return children;
};
export default PrivateRoute;
